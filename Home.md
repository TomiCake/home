![Survival Games X][headerIMG]

Welcome to the wiki! This is the documentation for the [Survival Games X plugin][pluginURL] found on [bukkit][bukkitURL]. Here you can find all the information you need to run a full functional survival games server.

[TOC]

## Setting up your First Arena

It's actually quite easy to get your first arena setup if you know what your doing. The [Setup Page](setup) will show you exactly how to do this.

## Adding Worlds

Adding worlds is easy. All you need to do is add the world in the server folder (where the other worlds are) then add the world to the config.

### Using the Config

To add a world to the **Arenas.cfg** file all you need to do is add the name of the world (case sensitive) then reload/restart the plugin.
You can reload the plugin by typing ``/sg reload`` as an OP or if you have the ``survivalgames.admin.reload`` permission.

```
#!ruby
#plugin/Arenas.cfg

# -- Worlds --

lobby { # This is the lobby info. We can ignore this.
    worldFolder = 'lobby'
    lobbyName = 'Lobby'
    lobbyTime = '240'   # Lobby seconds.
    stateMessageTime = '40'   # How often the state message appears.
    adminChat = '&RED[ADMIN] %tdisplay%: %message%'   # The format of the chat for admins.
    spectatorChat = '&GRAY[SPECTATOR] %tdisplay%: %message%'   # The format of the chat for spectators.
    tributeChat = '&DARK_GREEN[%points%] %tdisplay%: %message%'   # The format of the chat for tributes.
}
worlds {
    arena01 { # This is my new arena.
    }
}

```

After you reload the arena should look like this.

```
#!ruby
#plugin/Arenas.cfg

worlds {
    arena01 {
        arenaName = 'Survival Games'
        enabled = 'true'
        graceTime = '30'   # Grace period seconds.
        gameCountdown = '60'   # Countdown seconds before game begins.
        gameTime = '20'   # Game minutes.
        deathMatchCountdown = '60'   # Countdown before deathmatch.
        deathMatchTime = '300'   # Death match seconds.
        minStartTributes = '6'   # The minimum amount of tributes needed to start the game.
        minDMTributes = '4'   # The amount of tributes that need to be left for the deathmatch to start.
        winPoints = '100'
        killPoints = '10'
        killPercent = '0'   # This amount will be rewarded from a player you killed.
        refillWorldTime = '17000'   # The minecraft world time that chests will be refilled. (In ticks)
        killDMRun = 'false'   # Kills the tribute when they run away.
        dmRange = '35.0'   # The distance in blocks before the player is teleported or killed in deathmatch.
        adminChat = '&RED[ADMIN] %tdisplay%: %message%'   # The format of the chat for admins.
        spectatorChat = '&GRAY[SPECTATOR] %tdisplay%: %message%'   # The format of the chat for spectators.
        tributeChat = '&DARK_GREEN[%points%] %tdisplay%: %message%'   # The format of the chat for tributes.
    }
}

```

### Using Commands

Adding worlds using commands is very simple. All you need is one command. ``/sg addarena [worldName]``
The plugin will then take care of the rest for you.

## Customizing your Worlds
### Setting up Chest/Container Items

Rewards are a neccessity in Survival Games. Without rewards there is no survival.

To have rewards you need to declare what kind of block or "containers" will be used as the chest. You can have multiple containers spawning many different rewards. A good way to use this functionality is to have reward tiers. So a normal chest can have ordinay survival games item while a trap chest can hold more valuable rewards.

Lets go through how to setup a container. Add your first container. In this case I called it "chest", this must be the block id or name you wish to use as the container.

```
#!ruby
# plugin/worldName/Rewards.cfg

containers {

    chest {   # Added this line and the curly brace below.
	}

}

```

Now reload the plugin using the ``sg reload`` in the console. (``/sg reload`` in-game)

You will now see more options for the container.

```
#!ruby


containers {
    chest {
        enabled = 'true'   # If false then this container will not be used.
        name = 'Chest'   # The title that appears for the inventory.
        minChestRewards = '0'   # The min amount of rewards that can be in a chest.
        maxChestRewards = '15'   # The max amount of rewards that can be in a chest.
        rewards {
        	# Add rewards here.
    	}
	}
}

```

Alright you can now use chests but there are no rewards setup yet.

Lets go over what a reward looks like in the config.

Here is the format of the reward: ``[item id/name] ( : [data] ) = [rarity] ( x [amount] ( , [max] ) )``

Before you get confused, lets go over the format. ``[]`` are required. ``()`` are optional.

* The first section ``[item id/name]`` is pretty simple. This is just the name or id of the item you want to give as a reward. Here is a list of [Item Names][materialRef], just remove the underscore in the names. (``LAPIS_BLOCK`` = ``lapisblock``) Optionally you can add a data value to the reward by placing a colon then the data number afterwards. (``wool:14`` wool is the item and 14 is the data value)

* Next is the rarity of the reward. This is between 0 and 100. 0 meaning never and 100 meaning all the time. (``wool:14 = 50`` 50 is the rarity)

* At this point the reward will actually work. The plugin knows you want an item that spawns with a given rarity and since there is no amount given it assumes you only want 1. However in most cases you want to control how many rewards spawn. You can do this by specifying a number after the ``x``. (``wool:14 = 50 x 12`` 12 is the amount of wool)

* What if you want to have a random amount of the reward in a container? All you need to do is specify a min and max instead of just an amount. (``wool:14 = 50 x 12, 64`` 12 is the min and 64 is the max.)

We can now place our reward into the container config.
```
#!ruby


containers {
    chest {
        enabled = 'true'
        name = 'Chest'
        minChestRewards = '0'
        maxChestRewards = '15'
        rewards {

        	# Red wool with a rarity of 50% and a random amount between 12 and 64.
        	wool:14 = 50 x 12, 64

    	}
	}
}

```

Now what if you want to use a block instead of a chest? 
Well you can and it's setup the same way a chest is. A commonly used block is the ``crate`` that has an id of ``33:7``.


```
#!ruby


containers {
    chest {
        enabled = 'true'
        name = 'Chest'
        minChestRewards = '0'
        maxChestRewards = '15'
        rewards {
            stonesword = '30'
            flintandsteel = '10'
            chainmailchestplate = '15'
            chainmailhelmet = '15'
            chainmailleggings = '15'
            diamond = '5'
            stick = '20'
        }
    }

    # You can have multiple containers!
    33:7 { # This is a crate.

        enabled = 'true'
        name = 'Crate'
        minChestRewards = '5'
        maxChestRewards = '15'
        rewards {
        	wool:14 = 50 x 12, 64
    	}
	}
}

```

### Setting up Spawns

A spawn point is a place where the player will be teleported to at the beginning of the game. You can have as many spawns as you want, however keep in mind that the amount of people that are allowed to play is determined by how many spawns you have.

#### Using the Config
    
Setting up spawns in the config is more difficult but allows for precise spawn points. There are four things you need for a spawn point:

* The ``[index]`` which tells the plugin how to order the spawns. The order of the spawns important because the plugin seperates the players evenly along the spawns when the game starts. The ``[index]`` can have any number because the plugin just orders them when it's loaded.
* The ``[x]`` position of the spawn point.
* The ``[y]`` position of the spawn point.
* The ``[z]`` position of the spawn point.

The format looks like this: ``[index] = [x], [y], [z]``

An example of a spawn point looks like this:
``1 = -1576.35, 60.0, -585.52``

If you just added your world, your ``Rewards.cfg`` will look like this:

```
#!ruby

# -- Arena Spawns --

spectatorSpawn = '0,0,0' # This will default to the world spawn.

spawns {
    # I added our new spawn here.
    1 = -1576.35, 60.0, -585.52
}

# -- Deathmatch Spawns --

dmCenter = '0,0,0'   # Used in deathmatch to keep players in.
dmSpectatorSpawn = '0,0,0'
# Leave everything blank to use arena spawns for deathmatch.
deathmatch spawns {
}


```

A ordinary arena has 24 spawn points in the center of the map so you would just repeat the spawn point below the previous one except with a different index.

#### Using the Commands.

If you don't need to be precise with the spawn points it is recommended you use the commands.

First you will need to begin editing the arena using the edit command ``/sg edit [worldName]``. See the [World Editting Commands][worldEditting] section.

You can add a spawn point by moving your character to the spot you want the spawn point and typing ``/sg setspawn [index]``. For your convenience you can use the world ``next`` in place of ``[index]`` in the command. This will increment the index automatically. 

If you misplaced a spawn point just type the command again with the index you want to change.

You can type ``/sg delspawn [index]`` to remove any unwanted spawn points. For example, you place too many.

If you need to see what spawns already exist you can type ``/sg spawns`` to get a list of the spawns.

If you need to see where the spawn point is you can use the ``/sg tpspawn [index]`` command to teleport to the spawn.

You can also set the spectator spawn using the ``/sg setspecspawn`` command. This defaults to the world spawn.

Setting **deathmatch** spawns is very similar. You do not need to set deathmatch spawns, it will use the normal spawns for deathmatch if you don't.

The commands for deathmatch are:

* ``/sg setdmspawn [index | "next"]``
* ``/sg deldmspawn [index]``
* ``/sg dmspawns``
* ``/sg tpdmspawn [index]``
* ``/sg setdmspecspawn``

**If you do set a different area for deathmatch you need to set the deathmatch center.**
You can do this with the ``/sg setdmcenter`` command.

Don't forget to save the arena when your done! ``/sg save``

### Setting up Block Whitelist
### Adding Multiple containers
### Examples

## Customizing your Language File
### Adding Color
### Variables Explained
### Making Multiline Languages
### Example

## Using the Commands and Permissions
### Voting Commands
#### Vote Permissions
### Game Manipulation Commands
### World Editing Commands

World editing is a convenience feature that allows you to place/break blocks and set spawn points and other points in-game.

You enter edit mode when you type the ``/sg edit [worldName]`` command.

See the [Spawn Commands][spawnCommands] section to edit spawns and other points.

After you have finished your edits to the arena type ``/sg save`` to save the arena and exit to the lobby.

### User Control Commands
### Advanced Debugging and Configuration

## Setting up Auto-Restart
## Setting up Your Server List MOTD
## Setting up MySQL
### How to Use the Data Stored in MySQL

## Understanding the Plugin
### How Points Work
### How Stats Work
### How the Plugin Resets the Worlds

## Configuring the Plugin to Work for Chat
## How to use a Seperate Plugin for Chat

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

[pluginURL]: http://dev.bukkit.org/bukkt-plugins/Survival-Games-X/
[bukkitURL]: http://www.bukkit.org/
[headerIMG]: images/Header.jpg "Survival Games X"
[materialRef]: http://jd.bukkit.org/rb/doxygen/d6/d0e/enumorg_1_1bukkit_1_1Material.html
[worldEditting]: Home#markdown-header-world-editing-commands
[spawnCommands]: Home#markdown-header-using-the-commands