How i can Bypass it?

First you have to add in the folder of your world (e.x. ServerRoot/Arena01) the 
``level.dat``
to a 
``level.dat.zip``
and the 
``region folder`` 
to
``region.zip``.
Now you add the 
``level.dat.zip``
and the 
``region.zip``
to a zip called [YourMapName] (e.x. Arena01).
Now move the 
``Arena01.zip`` 
to the Server's root. Now you have bypassed this problem in 1.7.9. 

Have Fun! :)