![Survival Games X][headerIMG]

Welcome to the setup page! Here you will find help for when you set up your own Survival Games X server.

[TOC]

## Step 1 - Getting your bukkit server setup.

Bukkit has a nice guide that will help you setup your bukkit server. [Click here to visit bukkits setup page.][bukkitSetup]

## Step 2 - Getting the required dependancies.

Survival Games X required a couple plugins to work properly. The first is [Vault][vaultURL], this is used to allow all kinds of economy and permission plugins to work with SGX. The second is [VanishNoPacket][vanishURL], this plugin is used to hide spectators and reduce the amount of data the server needs to send. VanishNoPacket is not required but removing this plugin will disable spectating.

Download both of these plugins and place them in your plugins folder along side the survival games x plugin.

## Step 3 - Generating the files needed for the plugin.

All you need to do to generate the plugin files is run the server. After that you will see a new folder in your plugins directory called ``SurvivalGames``.

## Step 4 - Adding a world.

Adding a world is easy and you can see how [here][addWorld].

## Step 5 - Setting up the world spawns.

Setting the spawns is easy and you can see how [here][spawnCommands].

## Step 6 - Setting up the world rewards.
## Step 7 - Setting up the block whitelist.
## Step 8 - Making sure your world is enabled.
## Step 9 - Testing your new world.
## All done.

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax.

[pluginURL]: http://dev.bukkit.org/bukkt-plugins/Survival-Games-X/
[bukkitURL]: http://www.bukkit.org/
[headerIMG]: images/Header.jpg "Survival Games X"
[materialRef]: http://jd.bukkit.org/rb/doxygen/d6/d0e/enumorg_1_1bukkit_1_1Material.html
[addWorld]: Home#markdown-header-adding-worlds
[spawnCommands]: Home#markdown-header-using-the-commands
[vaultURL]: http://dev.bukkit.org/bukkit-plugins/vault/
[vanishURL]: http://dev.bukkit.org/bukkit-plugins/vanish/
[bukkitSetup]: http://wiki.bukkit.org/Setting_up_a_server